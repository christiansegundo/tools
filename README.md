# README #

THESE FILES ARE PROVIDED AS-IS WITHOUT ANY WARRANTY, EXPRESSED NOR
IMPLIED.

**USE AT YOUR OWN RISK!**


### About ###

Here are some scripts I've developed/use on a daily basis.

Currently included:

 - bat-applet : Applet to display battery level and state.
 - clipboard_history : Keeps track of your X clipboard.
 - ipinfo : Geolocate IP address using ipinfo.io.
 - kbd_brightness : Control keyboard backlight (used on my Macbook with xbindkeys).
 - lcd_brightness : Control screen brightness (used on my Macbook with xbindkeys).
 - namemark : DNS benchmarking utility.
 - pa-applet : Applet to control PulseAudio.
 - rename : Larry's filename fixer from Perl Cookbook.
 - toutf8 : Convert text files to utf8.
