#!/usr/bin/perl -w
use strict;
use File::Basename;
use Glib;
use Gtk2 '-init';

use constant TIMEOUT => 5000;
use constant PS_DIR => '/sys/class/power_supply/';
use constant ICON_DIR => dirname(__FILE__) . '/icons/';

die "No battery found." unless my $batt = get_batt();

my ($status, $charge);

my %icons = (
	discharging_low 	=>   ICON_DIR . 'discharging_low.png',
	discharging_25		=>   ICON_DIR . 'discharging_25.png',
	discharging_50 		=>   ICON_DIR . 'discharging_50.png',
	discharging_75		=>   ICON_DIR . 'discharging_75.png',
	discharging_90		=>   ICON_DIR . 'discharging_90.png',
	discharging_full 	=>   ICON_DIR . 'discharging_full.png',
	charging_low		=>   ICON_DIR . 'charging_low.png',
	charging_25			=>   ICON_DIR . 'charging_25.png',
	charging_50			=>   ICON_DIR . 'charging_50.png',
	charging_75			=>   ICON_DIR . 'charging_75.png',
	charging_90			=>   ICON_DIR . 'charging_90.png',
	charging_full 		=>   ICON_DIR . 'charging_full.png',
);


my $icon = Gtk2::StatusIcon->new;
$icon->set_visible(1);

update();
Glib::Timeout->add(TIMEOUT, \&update);
Gtk2->main;

sub update {
	die unless ($status, $charge) = get_details();
	update_icon($status,$charge);
	return 1;
}

sub update_icon {
	my ($s, $p) = @_;
	#set tooltip before messing with value
	$icon->set_tooltip($p . '%');
	if($p > 95) { $p = 'full'; }
	elsif($p > 90) { $p = '90'; }
	elsif($p > 75) { $p = '75'; }
	elsif($p > 50) { $p = '50'; }
	elsif($p > 25) { $p = '25'; }
	else{ $p = 'low'; }
	$icon->set_from_file($icons{$s. '_' . $p});
	}

sub get_batt {
    opendir(DIR, PS_DIR) or die $!;
    while(my $dir = readdir(DIR)) {
		if($dir =~ /BAT\d/) {
			my $bat = $dir . '/';
			close(DIR);
			return $bat;
		}
	}
    close(DIR);
	return;
}

sub get_details {
	my ($status, $charge, $file);

	open $file, '<', PS_DIR . $batt . 'capacity' or die $!;
	chomp($charge = <$file>);
	close $file;
	return unless $charge && $charge < 100 && $charge > 0;

	open $file, '<', PS_DIR . $batt . 'status' or die $!;
	$status = <$file>;
	close $file;

	#Some laptops report Unkown status when charging
	#so anything but Discharging means Charning
	if($status && $status =~ /^Discharging/) { $status = 'discharging'; }
	elsif($status) { $status = 'charging'; }

	return ($status, $charge);
}
