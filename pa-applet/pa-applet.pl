#!/usr/bin/env perl
use warnings;
use strict;
use File::Basename;
use Gtk2 '-init';

use constant ICON_DIR => dirname(__FILE__) . '/icons/';

my %icons = (
	low 	=> ICON_DIR . 'low.png',
	med 	=> ICON_DIR . 'med.png',
	full 	=> ICON_DIR . 'full.png',
	mute 	=> ICON_DIR . 'mute.png');

my $popup = Gtk2::Window->new( 'toplevel' );
$popup->signal_connect('focus-out-event', sub{$popup->hide;});
$popup->set_decorated(0);
$popup->set_resizable(0);
$popup->set_size_request(40,150);

$popup->set_position( 'mouse' );


my $vbox = Gtk2::VBox->new( 0, 0 );
$vbox->set_border_width(2);

my $vscale = Gtk2::VScale->new_with_range(0,100,5);
$vscale->set_value(get_volume());
$vscale->set_inverted(1);
$vscale->signal_connect('value_changed', \&set_volume);

$vbox->pack_start($vscale, 1, 1, 0 );
$popup->add($vbox);

my $icon = Gtk2::StatusIcon->new;
$icon->signal_connect('popup-menu', \&iconmenu);
$icon->signal_connect('activate', sub{	$popup->get_visible ? $popup->hide : $popup->show_all;
										$vscale->set_value(get_volume()); });

update_icon(get_volume());
$icon->set_visible(1);

Gtk2->main;

sub move_sink_input {
	my ($input, $sink) = @_;
	`pactl move-sink-input $input $sink`;
}

#Returns array with sink-inputs number
sub get_sink_inputs {
	my $output = `pactl list sink-inputs`;
	my (@inputs, $index);
	foreach(split(/\n/, $output)) {
	($_ =~ /Sink Input #(\d+)/)&&($inputs[$index++] = $1);
	}
	return @inputs;
}

#Returns sink NAME if success
sub set_default_sink {
	my $label = shift || die "Sink label?";
	my ($index, $name_at) = (0,0);
	foreach(get_sinks()) {
		foreach(@$_) {
			($name_at)&&(next);
			$_ eq $label ? $name_at = $index - 1 : $index++;
		}
	}
	$index=0;
	foreach(get_sinks()) {
		foreach(@$_) {
			if($name_at eq $index) {`pactl set-default-sink $_`; return $_;}
			$index++;
		}
	}
}

sub toggle_sink {
 	my $menu_item = shift;
	my $name;
	($menu_item->get_active)||(return);
	($menu_item->get_active)&&($name=set_default_sink($menu_item->get_label));
	update_icon(get_volume()); #Just in case new sink has different levels
	#Move sink-inputs to the new sink
	foreach(get_sink_inputs()) { move_sink_input($_, $name); }
}

sub iconmenu {
	my ($icon, $button, $time) = @_;

	my $menu = Gtk2::Menu->new();
	my ($default, $e, $toggle_group, @array_of_toggle);
	my $i=0;

	foreach(get_sinks()) {
		foreach(@$_) {
			($_ eq get_default_sink())&&($default=1);
			next unless (++$e % 2 == 0); #Only add Description (label) entries, Name entry is only useful for the default sink
			$array_of_toggle[$i] = $toggle_group ? Gtk2::RadioMenuItem->new($toggle_group,$_) : Gtk2::RadioMenuItem->new(undef,$_);
			($i == 0)&&($toggle_group = $array_of_toggle[$i]->get_group());
			if($default) {
				$array_of_toggle[$i]->set_active(1);
				$default=0;
			}
			$menu->append($array_of_toggle[$i++]);
		}
	}

	foreach(@array_of_toggle) { $_->signal_connect('toggled', \&toggle_sink); }

	$menu->show_all;
	my ($x, $y, $push_in) = Gtk2::StatusIcon::position_menu($menu, $icon);
	$menu->popup(undef, undef, sub{return ($x,$y,0);} , undef, 0, $time);
}

sub update_icon {
	my $volume = shift;
	my $i;
	if($volume >= 100) { $i = 'full'; }
	elsif($volume >= 50) { $i = 'med'; }
	elsif($volume > 0) { $i = 'low'; }
	elsif($volume == 0) { $i = 'mute'; }
	$icon->set_from_file($icons{$i});
}

#RETURNS A NAME NOT A LABEL
sub get_default_sink {
	my $i = `pactl info`;
	$i =~ /Default Sink:\s(.*)/ ? return $1 : die "Default sink?";
}

#Returns a multidimensional array with sink name and description eg Name: $a[0][0] Description: $a[0][1]
sub get_sinks {
	my $output = `pactl list sinks`;
	my @sinks;
	my $index=0;
	my ($i,$a);

	foreach(split(/\n/, $output)) {
	if($_ =~ /Sink #\d+/) { $i = $index + 2; }
	elsif($i == $index) {
		$_ =~ s/\sName:\s//;
		$sinks[$a++][0]=$_;
	}
	elsif($i + 1 == $index) {
		$_ =~ s/\sDescription:\s//;
		$sinks[$a-1][1]=$_;
	}
	$index++;
	}

	return @sinks;
}

sub get_volume {
	my $sink = shift || get_default_sink();
	my $i = `pactl list sinks`;
	my ($index, $volume_at);
	my %sinks;

	foreach(split/\n/, $i) {
		next if $volume_at;
		$_ =~ /^\sName:\s$sink/ ? $volume_at = $index+7 : $index++;
	}

	die "Volume?" unless $volume_at;

	$index=0;
	foreach(split/\n/, $i) {
		if($index == $volume_at) {
			$_ =~ /Volume:\s.*?\d+\s+\/\s+(\d+)%/ ? return $1 : die "Volume?";
		} $index++;
	}
}

sub set_volume {
	my $i = shift;
	my $new_volume = $i->get_value;
	`pactl set-sink-volume \@DEFAULT_SINK\@ $new_volume%`;
	update_icon($new_volume);
}


